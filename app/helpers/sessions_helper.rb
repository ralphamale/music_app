module SessionsHelper

  def login_user!(user)
    user.reset_session_token
    session[:session_token] = user.session_token
    fail
    redirect_to user_url(user)
  end

  def current_user
    User.find_by_session_token(session[:session_token])
  end

  def logged_in?
    current_user.nil? ? false : true
  end

end
