class BandsController < ApplicationController

  def index

  end

  def show
    @band = Band.find(params[:id])
    @band_albums = @band.albums
    @band_tracks = @band.tracks
  end

  def new
    @band = Band.new
  end

  def create
    @band = Band.new(params[:band])

    if @band.save
      redirect_to band_url(@band)
    else
      render :json => @band.errors.full_messages
    end

  end

  def edit
    @band = Band.find(params[:id])
  end

  def update
    @band = Band.find(params[:id])
    if @band.update_attributes(params[:band])
      redirect_to band_url(@band)
    else
      render :json => @band.errors.full_messages
    end
  end

  def destroy

  end

end
