class AlbumsController < ApplicationController

  def index

  end

  def show
    @album = Album.find(params[:id])
    @album_tracks = @album.tracks
    @album_band = @album.band
  end

  def new
    @bands = Band.all
    @cur_band = Band.find(params[:band_id])
    @album = Album.new
  end

  def create
    @album = Album.new(params[:album])
    @album.save!
    redirect_to album_url(@album)
  end

  def edit
    @bands = Band.all
    @album = Album.find(params[:id])


    @cur_band = @album.band

  end

  def update
    @album = Album.find(params[:id])

    if @album.update_attributes(params[:album])
      redirect_to album_url(@album)
    else
      render :json => @album.errors.full_messages
    end

  end

  def destroy

  end

end
