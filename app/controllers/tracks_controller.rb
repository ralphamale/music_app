class TracksController < ApplicationController

  def index

  end

  def show

    @track = Track.find(params[:id])
    #should link to the Album and Band.
    @track_album = @track.album
    @track_band = @track.band

  end

  def new
    @albums = Album.all
    @track = Track.new
    @cur_album = Album.find(params[:album_id])
  end

  def create
    @track = Track.new(params[:track])
    if @track.save
      redirect_to track_url(@track)
    else
      render :json => @track.errors.full_messages
    end
  end

  def edit
    @albums = Album.all
    @track = Track.find(params[:id])
    @cur_album = @track.album
  end

  def update
    @track = Track.find(params[:id])
    if @track.update_attributes(params[:track])
      redirect_to track_url(@track)
    else
      render :json => @track.errors.full_messages
    end
  end

  def destroy

  end

end
