class UsersController < ApplicationController

  def new
    @user = User.new
  end

  def index

  end

  def create
    @user = User.new(params[:user])
    @user.save!

    msg = UserMailer.activation_email(@user)
    msg.deliver!

    login_user!(@user)
  end

  def show
    @user = User.find(params[:id])
  end

  def activate
    @user = User.find_by_activation_token(params[:activation_token])

    if @user.nil?
      render :json => @user.errors.full_messages
    else
      @user.activated == true
      redirect_to user_url(@user)
    end
  end

end
