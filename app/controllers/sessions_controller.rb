class SessionsController < ApplicationController

  def new

  end

  def create
    @user = User.find_by_credentials(params[:user][:email], params[:user][:password])

    if @user.nil?
      raise "wrongo!" #change
    elsif @user.activated == false
      raise "Please activate"
    else
      login_user!(@user)
    end
  end

  def destroy
    @user = current_user
    session[:session_token] = nil
    @user.reset_session_token
    redirect to users_url
  end
end
