class Album < ActiveRecord::Base
  attr_accessible :band_id, :category, :name

  belongs_to :band,
  foreign_key: :band_id

  has_many :tracks, :dependent => :destroy,
  foreign_key: :album_id

end
