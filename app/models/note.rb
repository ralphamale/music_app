class Note < ActiveRecord::Base
  belongs_to :user,
  foreign_key: :user_id

  belongs_to :track,
  foreign_key: :track_id
end
