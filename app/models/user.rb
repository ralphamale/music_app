class User < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_reader :password
  attr_accessible :email, :password
  after_initialize :ensure_session_token
  after_initialize :ensure_activation_token

  has_many :notes,
  foreign_key: :user_id

  def password=(password)
    self.password_digest = BCrypt::Password.create(password)
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def ensure_activation_token
    self.activation_token ||= SecureRandom::urlsafe_base64(16)
  end

  def ensure_session_token
    self.session_token ||= SecureRandom::urlsafe_base64(16)
  end

  def reset_session_token
    self.session_token = SecureRandom::urlsafe_base64(16)
  end

  def self.find_by_credentials(email, password)
    user = User.find_by_email(email)

    if user.is_password?(password)
      user
    else
      nil
    end
  end

end
