class Track < ActiveRecord::Base
  attr_accessible :album_id, :category, :name, :lyrics

  belongs_to :album,
  foreign_key: :album_id

  has_one :band, through: :album, source: :band

  has_many :notes,
  foreign_key: :track_id
end

