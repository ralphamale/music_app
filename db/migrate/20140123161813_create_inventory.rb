class CreateInventory < ActiveRecord::Migration
  def change
    create_table :bands do |t|
      t.string :name, null: false

      t.timestamps
    end

    add_index :bands, :name, unique: true

    create_table :albums do |t|
      t.integer :band_id
      t.string :type, null: false
      t.string :name, null: false

      t.timestamps
    end

    add_index :albums, :band_id
    add_index :albums, [:type, :name], unique: true

    create_table :tracks do |t|
      t.integer :album_id
      t.string :name, null: false
      t.string :type, null: false
      t.text :lyrics, null: false

      t.timestamps
    end

    add_index :tracks, :album_id
    add_index :tracks, [:name, :type], unique: true


  end
end
