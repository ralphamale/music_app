class FixTypes < ActiveRecord::Migration
  def change
    remove_column :tracks, :type
    add_column :tracks, :category, :string

    remove_column :albums, :type
    add_column :albums, :category, :string
  end
end
